import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Test {

	public static void main(String[] args) throws Exception {
		Thread t = new Thread() {

			@Override
			public void run() {
				try {
					Class.forName("john.walker.spi.MysqlProxyDriver");
					Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/demo", "root", "root");
					test(con);
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			private void test(Connection con) throws Exception {
				PreparedStatement stmt = con .prepareStatement("select * from dd where id = ? and hire_date = ? limit 10");
				stmt.setInt(1, 1);
				stmt.setString(2, "2016-11-23");
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					System.out.println(rs.getString(1));
				}
				rs.close();
				stmt.close();
			}
		};
		t.start();
	}
}
